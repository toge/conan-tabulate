from conans import ConanFile, tools
import shutil

class TabulateConan(ConanFile):
    name           = "tabulate"
    version        = "1.3"
    license        = "MIT"
    author         = "toge.mail@gmail.com"
    url            = "https://bitbucket.org/toge/conan-tabulate/"
    homepage       = "https://github.com/p-ranav/tabulate/"
    description    = "Table Maker for Modern C++ "
    topics         = ("table", "text interface", "TUI")
    no_copy_source = True

    def source(self):
        tools.get("https://github.com/p-ranav/tabulate/archive/v{}.zip".format(self.version))
        shutil.move("tabulate-{}".format(self.version), "tabulate")

    def package(self):
        self.copy("*.hpp", "include", "tabulate/include")
