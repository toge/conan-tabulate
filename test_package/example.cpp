#include <tabulate/table.hpp>

int main() {
    tabulate::Table readme;
    readme.format().border_color(tabulate::Color::yellow);

    return 0;
}
